package com.sopra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.hpe.adm.nga.sdk.APIMode;
import com.hpe.adm.nga.sdk.Octane;
import com.hpe.adm.nga.sdk.authentication.Authentication;
import com.hpe.adm.nga.sdk.authentication.SimpleClientAuthentication;
import com.hpe.adm.nga.sdk.authentication.SimpleUserAuthentication;
import com.hpe.adm.nga.sdk.entities.EntityList;
import com.hpe.adm.nga.sdk.entities.OctaneCollection;
import com.hpe.adm.nga.sdk.examples.CreateContextExample;
import com.hpe.adm.nga.sdk.examples.EntityExample;
import com.hpe.adm.nga.sdk.metadata.Metadata;
import com.hpe.adm.nga.sdk.model.EntityModel;
import com.hpe.adm.nga.sdk.model.FieldModel;
import com.hpe.adm.nga.sdk.model.LongFieldModel;
import com.hpe.adm.nga.sdk.model.ReferenceFieldModel;
import com.hpe.adm.nga.sdk.model.StringFieldModel;

@SuppressWarnings("ALL")
public class OctaneCOnnect {
	
	private EntityList entityList;

	public static void main (String [] args){
        new OctaneCOnnect().createContext();
    }

    public void createContext() {
        // two types of authentication
        // 1) API Key
//        Authentication clientAuthentication = new SimpleClientAuthentication("clientId", "clientSecret");

   	
        // 2) User/pass
        Authentication userPassAuthentication = new SimpleUserAuthentication("cmittal", "Welcome@123", APIMode.TechnicalPreviewAPIMode);
        //Authentication userPassAuthentication = new SimpleUserAuthentication("charu.mittal@soprasteria.com", "Sopra@1234", APIMode.TechnicalPreviewAPIMode);

        // get instance of Octane Builder
        final Octane.Builder octaneBuilder = new Octane.Builder(userPassAuthentication);

        // now we can add the server
        octaneBuilder.Server("http://almoctane.corp.ssg:8080/");//https://almoctane-eur.saas.microfocus.com
        // the sharedspace
        octaneBuilder.sharedSpace(356004);
        // the workspace
        octaneBuilder.workSpace(1002);

        // finally we build the context and get an Octane instance:

        Octane octane = octaneBuilder.build();
        System.out.println("Connection established");
       
        // octane.entityList("defects").get().limit(2).execute();
        
        entityList = octane.entityList("defects");
        OctaneCollection defects = entityList.get().execute();
        System.out.println(defects.getTotalCount());
//        System.out.println(entityList.at("7001").get().execute());
        
        createNewEntity();
                
        defects = entityList.get().execute();
        System.out.println(defects.size());
        
    }
    
    public void createNewEntity() {

        // create some fields
        // the name field is a simple string
        final FieldModel<String> nameField = new StringFieldModel("name", "newDefect");
        // the invested_hours field is a number - here depicted as long
        final FieldModel<Long> investedHoursField = new LongFieldModel("invested_hours", 3L);
        // the parent field is a reference.  That means that there is a mini entity within the reference
        // here we define the entity model on the fly
        final FieldModel<EntityModel> parentField =
                new ReferenceFieldModel("parent",
                        new EntityModel(new HashSet(Arrays.asList(
                                new StringFieldModel("type", "work_item_root"),
                                // the reference here is an example - you need to get the correct value using the SDK
                                new StringFieldModel("id", "1001")))));

        // the phase is another reference field
        final FieldModel<EntityModel> phaseField =
                new ReferenceFieldModel("phase",
                        new EntityModel(new HashSet(Arrays.asList(
                                new StringFieldModel("type", "phase"),
                                new StringFieldModel("id", "1001")
                        ))));

        final FieldModel<EntityModel> releaseField =
                new ReferenceFieldModel("release",
                        new EntityModel(new HashSet(Arrays.asList(
                                new StringFieldModel("type", "release"),
                                new StringFieldModel("id", "1002")
                        ))));

        final FieldModel<EntityModel> severityField =
                new ReferenceFieldModel("severity",
                        new EntityModel(new HashSet(Arrays.asList(
                                new StringFieldModel("type", "list_node"),
                                new StringFieldModel("id", "1002")
                        ))));
        
        final FieldModel<String> descField = new StringFieldModel("description", "newDefect-description");
        
        // create the fields
        final Set<FieldModel> entityFields = new HashSet(Arrays.asList(nameField, parentField));

        final EntityModel entityModel = new EntityModel(entityFields);

        final Collection<EntityModel> createdEntities =
                // set the context to create
                entityList.create()
                        // add the entity model
                        .entities(new ArrayList<>(Collections.singletonList(entityModel)))
                        // carry out the execution
                        .execute();
        
        System.out.println(createdEntities);
    
    }

}
